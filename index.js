var express = require('express'),
app = express(),// crear epress app
cors = require('cors'),
port = process.env.port || 4200, // puerto
bodyParser = require('body-parser');

app.use(cors());


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

var routes = require('./api/routers/TestRouter');

app.use(function(req, res, next){
    
    res.header("Acces-Control-Allow-Origin","*");
    res.header('Access-Control-Allow-Credentials', true);
    res.header("Acces-Control-Allow-Headers", "Origin, X-Requested-With, Content-type, Accept");
    res.header("Content-Type", "application/x-www-form-urlencoded");
    res.header("Acces-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header('Access-Control-Expose-Headers', ['Content-Type']);
    next();
});

routes(app);
app.listen(port);
console.log("server iniciado en el puerto "+port);