'use strict'
var SchemaObject = require('schema-object');

var Empleado = new SchemaObject({
    id: Number,
	nombre: String, // mandatory
    apellido: String, // optional
    fecha_inicio: String,
    departamento: Number,
    puesto: String,
    salario: Number
});

module.exports = Empleado;