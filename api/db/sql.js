var sql = require("mssql");

var config = {
    user:"DevUser01",
    password:"Tak**Dev$$01",
    server: '192.168.1.38',
    instanceName: 'DEVSQL',
    database: 'TEST',
    "options": {
        "encrypt": false,
        "enableArithAbort": false
        }
}

function request(query){
    return new Promise((resolve,reject) => {
        new sql.ConnectionPool(config).connect().then(pool => {
            return pool.request().query(query)
        }).then(result => {
            sql.close();
            resolve(result.recordsets);
        }).catch(err => {
            sql.close();
            reject(err);
        });
    });
}
module.exports.request = request;