'use strinct'
var Sql = require('../db/sql.js');
var SchemaObject = require('schema-object');
var Empleado = require('../models/Empleado');

function getData(req,res)
{
    var Query = "Select * from Empleado";
    let promise =Sql.request(Query);
    promise
        .then(function(result){
            //res.send(result);
            var string = JSON.stringify(result);
            var largo = string.length;
            var cut = string.substring(1, largo-1);
            var empleado = JSON.parse(cut);
            
            //res.status(200).send({empleado});//datos guardados
            res.json(empleado);
            console.log(empleado);
        },
        function(error){
            res.send(error);
        });
}

function getDataId(req,res)
{
    var id = req.params.id;
    var Query = "Select * from Empleado where id="+id;
    let promise =Sql.request(Query);
    promise
        .then(function(result){
            console.log(result.rows)
            var string = JSON.stringify(result);
            var largo = string.length;
            var cut = string.substring(2, largo-2);
            var empleado = JSON.parse(cut);
            res.json(empleado);
            console.log(empleado);
        },
        function(error){
            res.send(error);
        });
}

function SaveData(req,res)
{
    var params = req.body;
    var empleado = new Empleado({
        nombre: params.nombre,
        apellido: params.apellido,
        fecha_inicio: params.fecha_inicio,
        departamento: parseInt(params.departamento),
        puesto: params.puesto,
        salario: parseInt(params.salario)
    });
    
    var Query = "Insert into Empleado(nombre, apellido, fecha_inicio, departamento, puesto, salario) values ('"+empleado.nombre+"','"+empleado.apellido+"','"+empleado.fecha_inicio+"',"+empleado.departamento+",'"+empleado.puesto+"',"+empleado.salario+")";
    let promise =Sql.request(Query);
    promise
        .then(function(result){
            res.json("Ingresado exitosamente");
        },
        function(error){
            res.send(error);
        });
}


function UpdateData(req,res)
{
    var params = req.body;
    var empleado = new Empleado({
        id: params.id,
        nombre: params.nombre,
        apellido: params.apellido,
        fecha_inicio: params.fecha_inicio,
        departamento: parseInt(params.departamento),
        puesto: params.puesto,
        salario: parseInt(params.salario)
    });
    
    var Query = "Update Empleado set nombre = '"+empleado.nombre+"', apellido  = '"+empleado.apellido+"', fecha_inicio  = '"+empleado.fecha_inicio+"', departamento = "+empleado.departamento+", puesto  = '"+empleado.puesto+"', salario = "+empleado.salario+" where id ="+empleado.id;
    let promise =Sql.request(Query);
    promise
        .then(function(result){
            res.json("Modificado exitosamente");
            console.log("modificado ok");
        },
        function(error){
            res.send(error);
            console.log(error);
        });
}


function DeleteDataId(req,res)
{
    var id = req.params.id;
    var Query = "Delete from Empleado where id="+id;
    let promise =Sql.request(Query);
    promise
        .then(function(result){
            res.json("Dato eliminado exitosamente");
            console.log("eliminado ok");
        },
        function(error){
            res.send(error);
            console.log(error);
        });
}



module.exports = {
    getData,
    getDataId,
    SaveData,
    UpdateData,
    DeleteDataId
}

