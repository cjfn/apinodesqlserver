var test = require('../controller/TestController');

module.exports = function(app){
    app.route('/test').get(test.getData);
    app.route('/test/:id?').get(test.getDataId);
    app.route('/test').post(test.SaveData);
    app.route('/test/:id').put(test.UpdateData);
    app.route('/test/:id').delete(test.DeleteDataId);
}